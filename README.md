# fullstack-php-app
  
  
	This pipeline will build the application container for all commits and push it to the docker registry.  
But it will only deploy the changes in master branch.  
  
  
How to run:  
  
1. Replace the required parameters in pdo application config file pdo/config.php  
	$host       = "mysql";  
	$username   = "root";  
	$password   = "password";  
	$dbname     = "test";  
  
2. Create a ssh key pair and add the public key to kubernetes master node  
   Add the private key to repository root  
  
3. In .gitlab-ci.yml replace follwoing parameters with relevant values  
		DOCKER_USERNAME: xxxxx  
  		DOCKER_PASSWORD: xxxxx  
		KUBERNETES_MASTER_IP: x.x.x.x  
  For deployeing in first time set IS_FIRST_DEPLOYMENT="true"	and for later deployments set IS_FIRST_DEPLOYMENT="false"  

